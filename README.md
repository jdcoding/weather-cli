# weather-cli

A small command-line tool to request weather information from a web-based source.
You can find the documentation of this project 
[here](https://jdcoding.gitlab.io/weather-cli/).


