import json
import os

import click

from weather_cli.main import (
    get_weather_data_from_rapid_api,
    format_response,
    save_output,
    api_key_path,
)


@click.group()
def click_main():
    """
    Simple command line tool to request weather information for a location of your
    choice.

    weather-cli COMMAND OPTIONS
    """
    pass


@click_main.command()
@click.argument("location")
@click.option(
    "--save/--no-save",
    default=False,
    help="Saves the weather information to the current working directory.",
)
def get_weather(location: str, save: bool):
    """
    Requests current weather information for LOCATION.
    """

    output = format_response(get_weather_data_from_rapid_api(location))
    print(json.dumps(output, indent=4))
    if save:
        save_output(output, os.getcwd(), location)


@click_main.command()
@click.argument("key")
def update_api_key(key: str):
    """
    KEY is the key to be used to request weather information from WeatherAPI
    """
    assert isinstance(key, str)
    expected_key_length = 50
    assert expected_key_length == len(
        key
    ), f"The key does not have the expected length of {expected_key_length}"
    with open(api_key_path, "w") as f:
        f.write(key)
    print("API key successfully updated!")
