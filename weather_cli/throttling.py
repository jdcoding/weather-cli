import time
from functools import partial, wraps


class ThrottlingException(Exception):
    """Can't be called due to throttling constraint"""

    pass


class CoolDownDecorator(object):
    def __init__(self, func, interval):
        self.func = func
        self.interval = interval
        self.last_run = 0

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self.func
        return partial(self, obj)

    def __call__(self, *args, **kwargs):
        now = time.time()
        if now - self.last_run < self.interval:
            remaining_seconds = self.last_run + self.interval - now
            raise ThrottlingException(f"Call after {remaining_seconds} seconds")
        else:
            self.last_run = now
            return self.func(*args, **kwargs)


def cool_down(interval):
    def apply_decorator(func):
        decorator = CoolDownDecorator(func=func, interval=interval)
        return wraps(func)(decorator)

    return apply_decorator
