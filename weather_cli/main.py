import json
import os
import time
from collections import OrderedDict
from typing import Union

import requests

from weather_cli.throttling import cool_down


cool_down_seconds = 1
example_response_path = os.path.join(
    os.path.split(__file__)[0], "example_response.json"
)
api_key_path = os.path.join(os.path.split(__file__)[0], "../rapid-api-key")


@cool_down(cool_down_seconds)  # throttle the call to this function to avoid
# accidentally overload the API
def get_weather_data_from_rapid_api(location: str) -> OrderedDict:
    url = "https://rapidapi.p.rapidapi.com/forecast.json"
    querystring = {"q": location, "days": "3"}
    with open(api_key_path) as f:
        api_key = f.readline().rstrip()
    headers = {
        "x-rapidapi-key": api_key,
        "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
    }
    request_response = requests.request("GET", url, headers=headers, params=querystring)
    response_dict = json.loads(request_response.text, object_pairs_hook=OrderedDict)
    with open(example_response_path) as f:
        expected_response = json.load(f, object_pairs_hook=OrderedDict)
    check_keys(expected_response, response_dict)
    return response_dict


def format_response(rapid_api_response: OrderedDict) -> OrderedDict:
    output_keys = {
        "location": ["name", "region", "country"],
        "current": ["temp_c", "condition", "wind_kph", "humidity", "gust_kph"],
        "forecast": [
            "maxtemp_c",
            "mintemp_c",
            "maxwind_kph",
            "totalprecip_mm",
            "avghumidity",
            "condition",
        ],
    }
    return OrderedDict(
        {
            k: filter_forecast_field(k, v, output_keys)
            for (k, v) in rapid_api_response.items()
            if k in output_keys.keys()
        }
    )


def filter_forecast_field(k: str, v: OrderedDict, keys: dict) -> OrderedDict:
    if k in ["location", "current"]:
        return parse_response_section(v, keys[k])
    elif k == "forecast":
        return OrderedDict(
            {
                day_dict["date"]: parse_response_section(day_dict["day"], keys[k])
                for day_dict in v["forecastday"]
            }
        )
    raise ValueError(f"Unexpected field in API response: {k}")


def parse_response_section(response_section: OrderedDict, keys: list) -> OrderedDict:
    return OrderedDict(
        {
            k2: filter_condition_field(k2, v2)
            for k2, v2 in response_section.items()
            if k2 in keys
        }
    )


def filter_condition_field(
    k: str, v: Union[OrderedDict, str]
) -> Union[OrderedDict, str]:
    if k == "condition" and isinstance(v, OrderedDict):
        return v["text"]
    else:
        return v


def save_output(output: OrderedDict, directory: str, request_string: str) -> None:
    timestamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
    file_name = request_string + "-" + timestamp + ".json"
    path = os.path.join(directory, file_name)
    with open(path, "w") as f:
        json.dump(output, f, indent=4)
    print(f"Saved output to {path}")


def check_keys(expected_dict: OrderedDict, actual_dict: OrderedDict) -> None:
    """
    Checks if keys of expected_dict are also part of actual_dict
    """
    for k, v1 in expected_dict.items():
        assert k in actual_dict
        if isinstance(v1, OrderedDict):
            v2 = actual_dict[k]
            assert isinstance(v2, OrderedDict)
            check_keys(v1, v2)
