.. _usage:

Usage
=====

Retrieve weather information
----------------------------
Execute the tool with ``weather-cli get-weather <location of your choice>``.
If you want to save the output to the current working directory, you can append the
flag ``--save``

Update API key
--------------
In case you want to update your API key, you can do this with the command
``weather-cli update-api-key <new API key>``
