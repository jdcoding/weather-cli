.. weather-cli documentation master file, created by
   sphinx-quickstart on Sat Nov 14 21:13:20 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to weather-cli's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   weather_cli
