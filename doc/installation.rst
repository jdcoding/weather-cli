Installation
============

1. Install the package
----------------------

We recommend to use some kind of virtual environment, e.g.
`conda <https://docs.conda.io/en/latest/>`_, to keep the required dependencies
encapsulated from other python applications.
In case of conda, this can be done by running

.. code-block:: bash

    conda create -n weather-cli python=3.6
    conda activate weather-cli

inside a conda prompt.
To install ``weather-cli``, then run:

.. code-block:: bash

    pip install Weather-CLI2 --extra-index-url https://gitlab.com/api/v4/projects/22183467/packages/pypi/simple

2. Get an API key
-----------------
This command line tool uses the
`WeatherAPI <https://rapidapi.com/weatherapi/api/weatherapi-com>`_ in the background.
You thus need an application key for `rapidapi.com <https://www.rapidapi.com>`_ to use
it.
To get one, create an account on the website and navigate to
``My Apps/default-application-<hash>/Security``.
You can access your  application key from there.

3. Subscribe to WeatherAPI
--------------------------
Navigate to `WeatherAPI <https://rapidapi.com/weatherapi/api/weatherapi-com>`_ and click
on ``Subscribe to Test``. Then select the free plan.

4. Setup you API key
---------------------
Configure weather-cli to use your API key by running ``weather-cli update-api-key
<API key>``

Weather API is now ready to use. Continue with the :ref:`usage` section.
