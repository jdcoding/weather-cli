FROM python:3.6

RUN pip install \
    bump2version \
    flake8 \
    pre-commit \
    pytest-mypy \
    sphinx-rtd-theme \
    twine
