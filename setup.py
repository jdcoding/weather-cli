#!/usr/bin/env python

import setuptools

setuptools.setup(
    name="Weather-CLI2",
    version="1.3.4",
    description="A command line tool to request weather data",
    author="Johannes Doellinger",
    author_email="johannes.doellinger@gmail.com",
    packages=["weather_cli"],
    install_requires=[
        "click",
        "requests",
    ],
    package_data={
        "weather_cli": ["py.typed", "example_response.json"],
    },
    python_requires=">=3.6",
    entry_points={
        "console_scripts": ["weather-cli=weather_cli.command_line_wrapper:click_main"]
    },
)
