import json
import os
import time
from collections import OrderedDict
from tempfile import TemporaryDirectory

import pytest

from weather_cli.main import (
    example_response_path,
    get_weather_data_from_rapid_api,
    format_response,
    cool_down_seconds,
    save_output,
)
from weather_cli.throttling import ThrottlingException


@pytest.mark.parametrize("location", ["Renningen", "Regensburg", "Berlin"])
def test_api(location):
    get_weather_data_from_rapid_api(location)
    time.sleep(cool_down_seconds)


def test_raise_throttling_exception():
    with pytest.raises(ThrottlingException):
        get_weather_data_from_rapid_api("Renningen")
        get_weather_data_from_rapid_api("Renningen")


example_output_path = os.path.join(os.path.split(__file__)[0], "example_output.json")


def test_format_response():
    with open(example_response_path) as f:
        example_return = json.load(f, object_pairs_hook=OrderedDict)

    with open(example_output_path) as f:
        example_output = json.load(f, object_pairs_hook=OrderedDict)

    assert example_output == format_response(example_return)


def test_save_response():
    with open(example_output_path) as f:
        example_output = json.load(f, object_pairs_hook=OrderedDict)
    with TemporaryDirectory() as directory:
        request_string = "Renningen"
        save_output(example_output, directory, request_string)
        files_in_directory = os.listdir(directory)
        assert 1 == len(files_in_directory)
        with open(os.path.join(directory, files_in_directory[0])) as f:
            assert example_output == json.load(f)
